"use strict";

const fs = require("fs");
const api = require("../api");
const specs = require("../specs");
const permit = require("../permit")
const validator = require("../validators");
const path = require("path");
const express = require("express");

const configure = (app, logger) => {
    const log = logger.start("settings:routes:configure");
    app.get("/specs", function(req, res) {
        fs.readFile("./public/specs.html", function(err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                });
            }
            res.contentType("text/html");
            res.send(data);
        });
    });

    app.get("/api/specs", function(req, res) {
        res.contentType("application/json");
        res.send(specs.get());
    });
    ////react js project setup////
    const root = path.join(__dirname, '../../startupbundle_bin_reactjs/', 'build')

    app.use(express.static(root));

    app.get('/*', function(req, res, next) {
        if (!req.path.includes('api')) {

            res.sendFile('index.html', { root });
        } else next();
    });

    //role api's //

    app.post(
        "/api/roles/create",
        permit.context.builder,
        validator.roles.create,
        api.roles.create
    );

    app.get(
        "/api/roles/getAll",
        permit.context.builder,
        api.roles.getRoles
    );

    app.delete(
        "/api/roles/deleteRole/:id",
        permit.context.validateToken,
        api.roles.deleteRole
    );

    log.end();
};

exports.configure = configure;